# substantially-small-serializer
Very lightweight, efficient, and small data serializer and deserializer for C# supporting JSON and possibly other languages in the future.

### Install

1. Go to [this site](https://www.nuget.org/packages/substantially_small_serializer/) for installation instructions.

2. Add this to the top of your source file
```cs
using SSSerializer;
using SSSerializer.Json;
```

### Data Structures (Nodes)

1. StringNode.
A simple string value.
```cs
var MyString = "hello";
var myStringNode = (StringNode)MyString;
```

2. ObjectNode.
A dictionary of string keys and Nodes.
```cs
var myObjectNode = new ObjectNode();
myObjectNode.Items.Add("key", (StringNode)"value");
```

3. ArrayNode.
A list of Nodes.
```cs
var myArrayNode = new ArrayNode();
myArrayNode.Items.Add((StringNode)"value");
myArrayNode.Items.Add(new ObjectNode {{ "key", (StringNode)"value" }});
```

### Node to JSON
Serialize a Node into a string containing json:
```cs
var myJson = new JSONWriter().Write(myNode, prettyPrint: true);
```

### JSON to Node
Deserialize a string containing json into a Node:
```cs
var myNode = new JSONReader().Read(myJson);
```
